#!/usr/bin/env bash
rebar get-deps
rebar compile
erlc \
	-o ebin \
	-pa deps/lager/ebin/ \
	-I include \
	-I ../ejabberd/include/ \
	-I deps/fast_xml/include/ \
	-I deps/lager/include/ \
    -DLAGER -DNO_EXT_LIB \
    src/*erl
