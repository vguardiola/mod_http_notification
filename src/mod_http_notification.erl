%%%------------------------------------------------------------------------------
%%% File    : mod_http_notification.erl
%%% Author  : Víctor Guardiola <victor@masquesoft.net>
%%% Purpose : Message, Offline Messages & presence passing via http to do actions
%%% Created : 04 Jun 2016 by Víctor Guardiola <victor@masquesoft.net>
%%%------------------------------------------------------------------------------

-module(mod_http_notification).
-author("victor@masquesoft.net").
-behaviour(gen_mod).

%% API
-export([start/2, stop/1, mod_opt_type/1, on_user_send_packet/4, on_offline_message/3, on_presence_update/4]).

-include("logger.hrl").
-include("ejabberd.hrl").
-include("jlib.hrl").

-define(DEFAULT_HTTP_HOST, <<"http://localhost">>).
-define(DEFAULT_PREFIX_PATH, <<"/">>).
-define(DEFAULT_HTTP_WORKER_TIMEOUT, 1000).

mod_opt_type(host) -> fun(A) -> A end;
mod_opt_type(prefix_path) ->fun(A) -> A end;
mod_opt_type(worker_timeout) -> fun (I) when is_integer(I), I > 0 -> I end;
mod_opt_type(_) -> [host, prefix_path, worker_timeout].

start(Host, _Opts) ->
    ejabberd_hooks:add(user_send_packet, Host, ?MODULE, on_user_send_packet, 100),
    ejabberd_hooks:add(offline_message_hook, Host, ?MODULE, on_offline_message, 50),
    ejabberd_hooks:add(unset_presence_hook, Host, ?MODULE, on_presence_update, 50),
    ?INFO_MSG("+++++++++ mod_http_notification:init", []),
    ok.

stop(Host) ->
  ejabberd_hooks:delete(user_send_packet, Host, ?MODULE, on_user_send_packet, 100),
  ejabberd_hooks:delete(offline_message_hook, Host, ?MODULE, on_offline_message, 50),
  ejabberd_hooks:delete(unset_presence_hook, Host, ?MODULE, on_presence_update, 50),
  ?INFO_MSG("+++++++++ mod_http_notification: end", []),
  ok.

on_user_send_packet(Packet, _C2SState, From, To) ->
  ?DEBUG("+++++++++ mod_http_notification: on_user_send_packet", []),
  Body = fxml:get_path_s(Packet, [{elem, <<"body">>}, cdata]),
  case should_make_req(Packet) of
    true ->
      post_message(From#jid.lserver, From#jid.luser, To#jid.luser, Body);
    _ ->
      ok
  end,
  Packet.

should_make_req(#xmlel{name = <<"message">>}) ->
  ?DEBUG("+++++++++ mod_http_notification: CHAT",[]),
  true;
should_make_req(_) ->
  false.

post_message(Host, Sender, Receiver, Message) ->
  ?DEBUG("Making request: ~s, ~s, ~s.", [Host, Sender, Receiver]),
  Query = <<"from=", Sender/binary, "&server=", Host/binary, "&to=", Receiver/binary, "&message=", Message/binary>>,
  Headers = [],
  do_request(Host, <<"messages">>, Headers, Query),
  ok.

on_presence_update(User, Host, _Resource, Status) ->
  ?DEBUG("+++++++++ mod_http_notification: on_presence_update", []),
  post_presence(User, Host, Status),
  ok.

post_presence(User, Host, Status) ->
  ?DEBUG("+++++++++ mod_http_notification: on_presence_update:request: ~s, ~s, ~s", [User, Host, Status]),
  Query = <<"from=", User/binary, "&server=", Host/binary, "&status=", Status/binary>>,
  Headers = [],
  do_request(Host, <<"presences">>, Headers, Query),
  ok.

on_offline_message(From, To, Packet) ->
  ?DEBUG("+++++++++ mod_http_notification: on_offline_message", []),
  Body = fxml:get_path_s(Packet, [{elem, <<"body">>}, cdata]),
  post_offline_message(From#jid.lserver, From#jid.luser, To#jid.luser, Body),
  ok.

post_offline_message(Host, From, To, Body) ->
  ?DEBUG("+++++++++ mod_http_notification: Posting From ~p To ~p Body ~p~n",[From, To, Body]),
  Query = <<"from=", From/binary, "&server=", Host/binary, "&to=", To/binary, "&message=", Body/binary>>,
  Headers = [],
  do_request(Host, <<"offlines">>, Headers, Query),
  ok.

do_request(Host, Url, Headers, Query) ->
  HttpHost = gen_mod:get_module_opt(Host, ?MODULE, host,fun(S) -> iolist_to_binary(S) end, ?DEFAULT_HTTP_HOST),
  Timeout = gen_mod:get_module_opt(Host, ?MODULE, worker_timeout,fun (I) when is_integer(I), I > 0 -> I end, ?DEFAULT_HTTP_WORKER_TIMEOUT),
  PathPrefix = gen_mod:get_module_opt(Host, ?MODULE, prefix_path, fun(S) -> iolist_to_binary(S) end, ?DEFAULT_PREFIX_PATH),
  HttpOptions = [{timeout, Timeout}],
  PostUrl =  erlang:bitstring_to_list(<<HttpHost/binary, PathPrefix/binary, Url/binary>>),
  ?DEBUG("+++++++++ mod_http_notification: Posting Url: ~s",[PostUrl]),
  {ok, _ReqId} = httpc:request(post, {PostUrl, Headers, "application/x-www-form-urlencoded", Query}, HttpOptions, []),
  ok.