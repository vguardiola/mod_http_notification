#mod_http_notification
This module enables forwarding certain events (messages, presence and offline messages) 
to an external service via HTTP, for example push (by mobile, email or SMS), big data,
or analytics services.

## How it works

The module registers a `user_send_packet`, `offline_message_hook` and `unset_presence_hook`
hooka,  every time some hook is triggered, the module does the following:

* checks whether http_notification is enabled (the `host` param is set)
* runs a callback module's depending the hook
* user_send_packet -> post_message(Host, Sender, Receiver, Message)
* offline_message_hook -> post_offline_message(Host, From, To, Body)
* unset_presence_hook -> post_presence(User, Host, Status)

## what do the methods?
All methods do a http request o a server configurate with `host` param and 
the `prefix_path`, and each method concatenate a personal path:

* post_message -> POST host+prefix_path+'messages'
* post_offline_message -> POST host+prefix_path+'offlines'
* post_presence -> POST host+prefix_path+'presences'

## Options

* `host`: this is where the HTTP notification server should listen.
* `prefix_path`: path part of URL to which a request should be sent.
* `worker_timeout`: how much time do we give HTTP server to process requests before we
bail out; default is 1000 ms.

## Example configuration
```yaml
modules:
  mod_http_notification:
    host: "http://localhost:8000"
    prefix_path: "/xmpp"
    worker_timeout: 5000
```


## To Do
* Pool
* add some other hook
* add filters to what send or not
* improve configuration (headers, extra data, ...)
* test
